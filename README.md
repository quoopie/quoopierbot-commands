# Description
A repo to hold all of the commands for my custom twitch bot!

# Twitch Channel to view this bot live

[Quoopier Twitch](https://twitch.tv/quoopier)

# Commands
!discord - Gives the link to my stream discord

!project - Tells what I am currently coding

!dice - Gives a random number between 1 and 6

!coinflip - Gives heads or tails

!commands - Gives the link to this repo

!submitidea \<idea text\> - Submits viewers ideas for me to look at later

!currentidea - Gives the current idea I am implementing

!completedideas - Gives the number of viewer ideas I have already implemented

!8ball \<Yes or no question\> - Gives a possible reply that 8balls can give

!github - Links to my gitlab since I do not use github

!gitlab - Links to my gitlab

!vod - Tells where people can find my vods
